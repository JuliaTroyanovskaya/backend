# README #

# KeepSolid BackEnd Lesson_1

[![Keep|Solid](https://s.dou.ua/CACHE/images/img/static/companies/logo_KS/03c31554021703fe5712a16d04d26b98.png)]()

Lesson1 is the "helloWord" app. The application represents 2 lincked Docker containers using docker-compose:
- Nginx
- PHP (PHP-FPM) with app

### SetUp
On Ubuntu
Lesson_1 requires [Docker](https://www.docker.com/) to run.

SetUp the dependencies and start the docker.

```sh
$ sudo docker-compose build
$ sudo docker-compose up -d
$ sudo docker-compose start
```

Checking processes ...

```sh
$ sudo docker-compose ps
```
Starting the app in the browser

127.0.0.1:8084

